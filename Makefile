all: build

build: main.c mem.c mem.h
	gcc -o main -ansi -std=c99 -pedantic -Wall -Werror main.c mem.c
clean:
	rm -f main

