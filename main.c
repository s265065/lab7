#include <stdio.h>
#include "mem.h"

int main(){

    size_t size = 1837192253;
    printf("Allocate %zu bytes...\n", size);
    void* addr = _malloc(size);
    printf("Free %p..\n", (void*) addr);
    _free(addr);
    return 0;
}