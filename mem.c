#include <unistd.h>
#include "mem.h"

struct mem* heapStart;

void* heap_init(size_t initial_size) {
    return _malloc(initial_size);
}

void* _malloc(size_t query) {
    if (query == 0) return NULL;
    if (query < _PAGESIZE) query = _PAGESIZE;
    struct mem* page = find_page(query);
    if (!page) page = create_new_page(query);
    return choose_page(page, query);
}

struct mem* find_page(size_t query) {
    struct mem* current = heapStart;
    while (current != NULL && (!current->is_free || current->capacity < query)) current = current->next;
    return current;
}

void* get_page_address(struct mem* page) {
    if (page == NULL)
        return NULL;
    int8_t* bodyStart = (int8_t*)(page + 1);
    return bodyStart + page->capacity;
}

struct mem* get_last_page() {
    struct mem* current = heapStart;
    if (current == NULL)
        return NULL;
    while (current->next)
        current = current->next;
    return current;
}

struct mem* create_new_page(size_t query) {
    struct mem* last = get_last_page();
    void* lastAddress = get_page_address(last);
    size_t length = sizeof(struct mem) + query;
    if (length < query) return NULL;
    if (lastAddress == NULL) lastAddress = HEAP_START;
    void* dedicatedAddress = mmap(lastAddress, length, MMAP_PROT, MAP_PRIVATE, -1, 0);
    if (dedicatedAddress == (void*) -1) return NULL;
    struct mem* new = dedicatedAddress;
    new->capacity = query;
    new->is_free = true;
    new->next = NULL;
    if (last != NULL) {
        last->next = new;
        bool merged = merge(last, new);
        if (merged) return last;
        return new;
    } else {
        heapStart = new;
        return new;
    }
}

void* choose_page(struct mem* page, size_t space) {
    if (page == NULL) return NULL;
    size_t prev_page_capacity = space;
    ssize_t next_page_capacity = page->capacity - space - sizeof(struct mem);
    page->is_free = 0;
    if (page->capacity != space && next_page_capacity >= _PAGESIZE) {
        page->capacity = prev_page_capacity;
        struct mem* new = get_page_address(page);
        new->capacity = next_page_capacity;
        new->is_free = 1;
        new->next = page->next;
        page->next = new;
    }
    return page + 1;
}


bool merge(struct mem* a, struct mem* b) {
    if (a == NULL || b == NULL) return false;
    if (!a->is_free || !b->is_free) return false;
    if (b->next == a) {
        struct mem* buffer = a;
        a = b;
        b = buffer;
    }
    if (a->next != b) return false;
    if (get_page_address(a) == b) {
        a->next = b->next;
        a->capacity += sizeof(struct mem) + b->capacity;
        return true;
    }
    return false;
}

void _free(void* memory) {
    for ( struct mem *current = heapStart, *previous = NULL; current; previous = current, current = current->next) {
        if (memory == current+1) {
            current->is_free = true;
            merge(previous, current);
            merge(current, current->next);
            break;
        }
    }
}