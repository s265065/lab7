#ifndef _MEM_H_
#define _MEM_H_
#define _USE_MISC_

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/types.h>
#define HEAP_START ((void*)0x04040000)
#define _PAGESIZE 4096

#define MMAP_PROT PROT_READ | PROT_WRITE

struct mem;

#pragma pack(push, 1)
struct mem{
    struct mem* next;
    size_t capacity;
    bool is_free;
};
#pragma pack(pop)

void* _malloc( size_t query );
void _free( void* memory );
void* heap_init( size_t initial_size );
struct mem* find_page(size_t query);
void* get_page_address(struct mem* page);
struct mem* create_new_page(size_t query);
struct mem* get_last_page();
bool merge(struct mem* a, struct mem* b);
void* choose_page(struct mem* page, size_t space);


#endif

